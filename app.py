from flask import Flask

app=Flask(__name__)


@app.route("/")
def home():
	return "hello world 2022"

@app.route("/test")
def sayHi():
	return "hi adam, welcome back"

@app.route("/test1")
def sayHi1():
	return "hi adam, welcome back again"


if __name__ == '__main__':
	app.run(debug=True,port=5002,host="0.0.0.0")